package fr.triozer.fastui;

import fr.triozer.api.plugin.TrioPlugin;
import fr.triozer.api.plugin.configuration.BaseConfiguration;
import fr.triozer.api.ui.ClickableItem;
import fr.triozer.api.ui.InventoryBuilder;
import fr.triozer.api.ui.ItemBuilder;
import fr.triozer.fastui.parser.FastInventory;
import fr.triozer.fastui.parser.FastItem;
import org.bukkit.Material;

import java.io.File;
import java.util.Arrays;

public final class FastUI extends TrioPlugin {

    private static FastUI instance;

    private FastItem.Manager itemManager;
    private FastItem.Parser  itemParser;

    private FastInventory.Manager inventoryManager;
    private FastInventory.Parser  inventoryParser;

    public static FastUI getInstance() {
        return instance;
    }

    @Override
    protected void enable() {
        instance = this;

        this.configuration = new BaseConfiguration(this) {
            @Override
            public void init() {
                File itemsFolder       = new File(this.plugin.getDataFolder(), "items");
                File inventoriesFolder = new File(this.plugin.getDataFolder(), "inventories");

                if (!itemsFolder.exists()) itemsFolder.mkdir();
                if (!inventoriesFolder.exists()) inventoriesFolder.mkdir();
            }
        };

        this.itemManager = new FastItem.Manager();
        this.itemParser = new FastItem.Parser();
        this.inventoryManager = new FastInventory.Manager();
        this.inventoryParser = new FastInventory.Parser();

        this.inventoryManager.add(new InventoryBuilder("Ceci est un test", 9 * 5, true)
                .fill(ClickableItem.empty(new ItemBuilder(Material.DIAMOND).name("lel").lore("what would you do?")))
                .setItem(5, ClickableItem.empty(new ItemBuilder(Material.DIAMOND))));

        this.console.warning(Arrays.toString(this.inventoryParser.parse(this.configuration.get("ceci-est-un-test")).getInventory().getContents()));
    }

    @Override
    protected void disable() {

    }

    public final FastItem.Manager getItemManager() {
        return this.itemManager;
    }

    public final FastItem.Parser getItemParser() {
        return this.itemParser;
    }

    public final FastInventory.Manager getInventoryManager() {
        return this.inventoryManager;
    }

    public final FastInventory.Parser getInventoryParser() {
        return this.inventoryParser;
    }

}
