package fr.triozer.fastui.parser;

import org.bukkit.configuration.file.YamlConfiguration;

/**
 * @author Cédric / Triozer
 */
public interface Parser<T> {

    T parse(YamlConfiguration configuration);

}
