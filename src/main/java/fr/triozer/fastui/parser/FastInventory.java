package fr.triozer.fastui.parser;

import fr.triozer.api.manager.AbstractManager;
import fr.triozer.api.ui.ClickableItem;
import fr.triozer.api.ui.InventoryBuilder;
import fr.triozer.api.ui.ItemBuilder;
import fr.triozer.fastui.FastUI;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * @author Cédric / Triozer
 */
public class FastInventory {

	public static class Manager implements AbstractManager<InventoryBuilder, String> {
		private final Map<String, InventoryBuilder> inventories;

		public Manager() {
			this.inventories = new HashMap<>();
		}

		@Override
		public InventoryBuilder get(String key) {
			return this.inventories.get(key);
		}

		@Override
		public void add(InventoryBuilder value) {
			this.inventories.put(value.getName(), value);
			String            name          = value.getName().replace(" ", "-").toLowerCase();
			YamlConfiguration configuration = FastUI.getInstance().getConfiguration().create(name, "inv.ui");

			configuration.set("name", value.getName());
			configuration.set("type", value.getInventory().getType().name());
			configuration.set("closeable", true);
			configuration.set("size", value.getSize());

			for (int i = 0; i < value.getInventory().getContents().length; i++) {
				ItemStack item = value.getInventory().getContents()[i];

				if (item == null || item.getType() == Material.AIR) {
					configuration.set("contents." + i + ".name", "undefined");
					configuration.set("contents." + i + ".type", "AIR");
					configuration.set("contents." + i + ".amount", 0);
					continue;
				}

				FastUI.getInstance().getItemParser().parse(new ItemBuilder(Material.DIAMOND), configuration, "contents." + i);

				if (item.hasItemMeta()) {
					if (item.getItemMeta().hasDisplayName())
						configuration.set("contents." + i + ".name", item.getItemMeta().getDisplayName());
					else
						configuration.set("contents." + i + ".name", "undefined");

					configuration.set("contents." + i + ".lore", item.getItemMeta().getLore());
				}
				configuration.set("contents." + i + ".type", item.getType().name());
				configuration.set("contents." + i + ".amount", item.getAmount());
			}

			FastUI.getInstance().getConfiguration().save(name);
		}

		@Override
		public void remove(InventoryBuilder value) {
			this.inventories.remove(value.getName());
			String name = value.getName().replace(" ", "-").toLowerCase();

			FastUI.getInstance().getConfiguration().remove(name);
		}

		@Override
		public Stream<InventoryBuilder> values() {
			return this.inventories.values().stream();
		}

	}

	public static class Parser implements fr.triozer.fastui.parser.Parser<InventoryBuilder> {

		@Override
		public InventoryBuilder parse(YamlConfiguration configuration) {
			String        name = configuration.getString("name");
			InventoryType type = null;
			int           size = 0;
			try {
				type = InventoryType.valueOf(configuration.getString("type"));
			} catch (IllegalArgumentException e) {
				size = configuration.getInt("size");
			}
			boolean closeable = configuration.getBoolean("closeable");

			InventoryBuilder inventoryBuilder;
			if (type == null)
				inventoryBuilder = new InventoryBuilder(name, size, closeable);
			else
				inventoryBuilder = new InventoryBuilder(name, type, closeable);

			ConfigurationSection contents = configuration.getConfigurationSection("contents");
			for (String slot : contents.getKeys(false)) {
				inventoryBuilder.setItem(Integer.parseInt(slot),
						ClickableItem.empty(FastUI.getInstance().getItemParser()
								.parse((YamlConfiguration) contents.getConfigurationSection(slot))));
			}
			return inventoryBuilder;
		}

	}

}
