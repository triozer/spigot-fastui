package fr.triozer.fastui.parser;

import fr.triozer.api.manager.AbstractManager;
import fr.triozer.api.ui.ItemBuilder;
import fr.triozer.fastui.FastUI;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * @author Cédric / Triozer
 */
public class FastItem {

    public static class Manager implements AbstractManager<ItemBuilder, String> {
        private final Map<String, ItemBuilder> items;

        public Manager() {
            this.items = new HashMap<>();
        }

        @Override
        public ItemBuilder get(String key) {
            return this.items.get(key);
        }

        @Override
        public void add(ItemBuilder value) {
            this.items.put(value.getName(), value);
            String            name          = value.getName().replace(" ", "-").toLowerCase();
            YamlConfiguration configuration = FastUI.getInstance().getConfiguration().create(name, "item.ui");

            FastUI.getInstance().getConfiguration().save(name);
        }

        @Override
        public void remove(ItemBuilder value) {
            this.items.remove(value.getName());
            String name = value.getName().replace(" ", "-").toLowerCase();

            FastUI.getInstance().getConfiguration().remove(name);
        }

        @Override
        public Stream<ItemBuilder> values() {
            return this.items.values().stream();
        }

    }

    public static class Parser implements fr.triozer.fastui.parser.Parser<ItemBuilder> {

        @Override
        public ItemBuilder parse(YamlConfiguration configuration) {
            String       name     = configuration.getString("name");
            Material     material = Material.valueOf(configuration.getString("material"));
            int          amount   = configuration.getInt("amount");
            List<String> lore     = configuration.getStringList("lore");

            return new ItemBuilder(material, amount).name(name).lore(lore.toArray(new String[0]));
        }

        public YamlConfiguration parse(ItemBuilder item, YamlConfiguration configuration, String path) {
            ItemStack itemStack = item.build();

            configuration.set(path + ".name", item.getName());
            configuration.set(path + ".material", item.getMaterial().name());
            configuration.set(path + ".amount", item.getAmount());
            if (itemStack.hasItemMeta() && itemStack.getItemMeta().hasEnchants())
                configuration.set(path + ".enchants", item.getEnchantments());

            configuration.set(path + ".data", item.getData());

            if (itemStack.hasItemMeta() && itemStack.getItemMeta().hasLore())
                configuration.set(path + ".lore", item.getLore());

            return configuration;
        }

    }

}
